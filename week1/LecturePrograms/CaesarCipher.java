
/**
 * Write a description of Decrypt here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class CaesarCipher {
    public String encrypt(String input, int key) {
        String alphabetU = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String alphabetL = "abcdefghijklmnopqrstuvwxyz";
        String keyAlphabetU = alphabetU.substring(key) + alphabetU.substring(0,key);
        String keyAlphabetL = alphabetL.substring(key) + alphabetL.substring(0,key);
        StringBuilder encrypted = new StringBuilder(input);
        for(int i = 0; i < encrypted.length(); i++) {
            char currChar = encrypted.charAt(i);
            if(Character.isUpperCase(currChar)) {
                int index = alphabetU.indexOf(currChar);
                if(index != -1) {
                    char newChar = keyAlphabetU.charAt(index);
                    encrypted.setCharAt(i,newChar);
                }
            }
            else {
                int index = alphabetL.indexOf(currChar);
                if(index != -1) {
                    char newChar = keyAlphabetL.charAt(index);
                    encrypted.setCharAt(i,newChar);
                }
            }
            
        }
        return encrypted.toString();
    }
    int[] countLetters(String message) {
        String alph = "abcdefghijklmnopqrstuvwxyz";
        int[] counts = new int[26];
        for(int k=0; k < message.length(); k++) {
            char ch = Character.toLowerCase(message.charAt(k));
            int dex = alph.indexOf(ch);
            if(dex != -1) {
                counts[dex] += 1;
            }
        }
        return counts;
    }
    public int maxIndex(int[] vals) {
        int maxDex = 0;
        for(int k=0; k <vals.length; k++) {
            if( vals[k] > vals[maxDex]) {
                maxDex = k;
            }
        }
        return maxDex;
    }
    public String decryption (String encrypted) {
        CaesarCipher cc = new CaesarCipher();
        int[] freqs = countLetters(encrypted);
        int maxDex = maxIndex(freqs);
        int dkey = maxDex - 4;
        if (maxDex < 4) {
            dkey = 26 - (4-maxDex);
        }
        return cc.encrypt(encrypted, 26 - dkey);
    }

}
