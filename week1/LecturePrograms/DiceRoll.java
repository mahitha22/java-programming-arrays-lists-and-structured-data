
/**
 * Write a description of DiceRoll here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.Random;
public class DiceRoll {
    public void simulate(int rolls) {
        Random rand = new Random();
        int counter[] = new int[13];
        for (int k = 0; k < rolls; k++) {
            int d1 = rand.nextInt(6) + 1;
            int d2 = rand.nextInt(6) + 1;
            System.out.println("roll is "+ d1 + "+" + d2 + "=" + (d1+d2));
            counter[d1+d2] += 1;
        }
        for (int k = 2; k <= 12; k++) {
            System.out.println(k + "'s=\t" + counter[k] + "\t" + 100.0 * counter[k]/rolls);
        }
    }
}
