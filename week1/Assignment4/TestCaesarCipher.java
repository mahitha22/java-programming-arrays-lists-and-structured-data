
/**
 * 
 * @version 11/08
 */
import edu.duke.*;
class CaesarCipher {
    private String alphabet;
    private String shiftedAlphabet;
    private int mainKey;
    CaesarCipher(int key) {
        alphabet = "abcdefghijklmnopqrstuvwxyz";
        shiftedAlphabet = alphabet.substring(key) + alphabet.substring(0,key);
        mainKey = key;
    }
    public String encrypt(String input) {
        StringBuilder encrypted = new StringBuilder(input);
        for(int i = 0; i < encrypted.length(); i++) {
            char currChar = encrypted.charAt(i);
            int index = alphabet.indexOf(Character.toLowerCase(currChar));
            if(index != -1) {
                if(currChar == Character.toLowerCase(currChar)) {
                    char newChar = shiftedAlphabet.charAt(index);
                    encrypted.setCharAt(i,newChar);
                }
                else {
                    char newChar = shiftedAlphabet.charAt(index);
                    encrypted.setCharAt(i,Character.toUpperCase(newChar));
                }
            }
        }
        return encrypted.toString();
    }
    public String decrypt(String input) {
        CaesarCipher cc = new CaesarCipher(26 - mainKey);
        return cc.encrypt(input);
    }
}
public class TestCaesarCipher {
    int[] countLetters(String message) {
        String alph = "abcdefghijklmnopqrstuvwxyz";
        int[] counts = new int[26];
        for(int k=0; k < message.length(); k++) {
            char ch = Character.toLowerCase(message.charAt(k));
            int dex = alph.indexOf(ch);
            if(dex != -1) {
                counts[dex] += 1;
            }
        }
        return counts;
    }
    public int maxIndex(int[] vals) {
        int maxDex = 0;
        for(int k=0; k <vals.length; k++) {
            if( vals[k] > vals[maxDex]) {
                maxDex = k;
            }
        }
        return maxDex;
    }
    public String breakCaesarCipher(String input) {
        int[] freqs = countLetters(input);
        int maxDex = maxIndex(freqs);
        int dkey = maxDex - 4;
        if (maxDex < 4) {
            dkey = 26 - (4-maxDex);
        }   
        CaesarCipher cc = new CaesarCipher(26-dkey);
        return cc.encrypt(input);
    }
    public void simpleTests() {
        FileResource fr = new FileResource();
        String message = fr.asString();
        CaesarCipher cc = new CaesarCipher(15);
        String encryptedMessage = cc.encrypt(message);
        System.out.println("Encrypted message is : "+encryptedMessage);
        String decryptMessage = cc.decrypt(encryptedMessage);
        System.out.println("decrypted Message is : "+ decryptMessage);
        String breakCaesarCipherMsg = breakCaesarCipher(encryptedMessage);
        System.out.println("Break CaesarCipher Message is : "+ breakCaesarCipherMsg);
    }
}