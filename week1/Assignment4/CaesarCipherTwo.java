
/**
 * Write a description of CaesarCipherTwo here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class CaesarCipherTwo {
    private String alphabet, shiftedAlphabet1, shiftedAlphabet2;
    private int mainKey1, mainKey2;
    CaesarCipherTwo(int key1, int key2) {
        alphabet = "abcdefghijklmnopqrstuvwxyz";
        shiftedAlphabet1 = alphabet.substring(key1) + alphabet.substring(0,key1);
        shiftedAlphabet2 = alphabet.substring(key2) + alphabet.substring(0,key2);
        mainKey1 = key1;
        mainKey2 = key2;
    }
    public String encrypt(String input) {
        StringBuilder encrypted = new StringBuilder(input);
        for(int i=0; i < encrypted.length(); i+=2) {
            char currChar = encrypted.charAt(i);
            int index = alphabet.indexOf(Character.toLowerCase(currChar));
            if(index != -1) {
                if(currChar == Character.toLowerCase(currChar)) {
                    char newChar = shiftedAlphabet1.charAt(index);
                    encrypted.setCharAt(i,newChar);
                }
                else {
                    char newChar = shiftedAlphabet1.charAt(index);
                    encrypted.setCharAt(i,Character.toUpperCase(newChar));
                }
            }
        }
        for(int i=1; i < encrypted.length(); i+=2) {
            char currChar = encrypted.charAt(i);
            int index = alphabet.indexOf(Character.toLowerCase(currChar));
            if(index != -1) {
                if(currChar == Character.toLowerCase(currChar)) {
                    char newChar = shiftedAlphabet2.charAt(index);
                    encrypted.setCharAt(i,newChar);
                }
                else {
                    char newChar = shiftedAlphabet2.charAt(index);
                    encrypted.setCharAt(i,Character.toUpperCase(newChar));
                }
            }
        }
        return encrypted.toString();
    }
    public String decrypt(String input) {
        CaesarCipherTwo cct = new CaesarCipherTwo(26 - mainKey1, 26- mainKey2);
        return cct.encrypt(input);
    }
}
