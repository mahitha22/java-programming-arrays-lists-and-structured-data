
/**
 * @version 11/8
 */
import edu.duke.*;
public class TestCaesarCipherTwo {
    private int[] countLetters(String message) {
        String alph = "abcdefghijklmnopqrstuvwxyz";
        int[] counts = new int[26];
        for(int k=0; k < message.length(); k++) {
            char ch = Character.toLowerCase(message.charAt(k));
            int dex = alph.indexOf(ch);
            if(dex != -1) {
                counts[dex] += 1;
            }
        }
        return counts;
    }
    private int maxIndex(int[] vals) {
        int maxDex = 0;
        for(int k=0; k <vals.length; k++) {
            if( vals[k] > vals[maxDex]) {
                maxDex = k;
            }
        }
        return maxDex;
    }
    private String halfOfString(String message, int start) {
        String temp = "";
        for(int i = start; i < message.length(); i += 2) {
            temp += message.charAt(i);
        }
        return temp;
    }
    private int getKey(String s) {
        int frequencies[] = countLetters(s);
        int maxindex = maxIndex(frequencies);
        return maxindex;
    }
    private void SampleTests() {
        FileResource fr = new FileResource();
        String message = fr.asString();
        CaesarCipherTwo caesarTwo = new CaesarCipherTwo(21,8);
        String encryptStr = caesarTwo.encrypt(message);
        System.out.println("Encrypted Message: "+encryptStr);
        System.out.println("Decrypted Message: "+caesarTwo.decrypt(encryptStr));
        System.out.println("Decrypted Message using breakCaesarCipher method: "+breakCaesarCipher(encryptStr));
    }
    public String breakCaesarCipher(String input) {
        String firstHalf = halfOfString(input,0);
        String secondHalf = halfOfString(input, 1);
        int maxDex1 = getKey(firstHalf);
        int maxDex2 = getKey(secondHalf);
        int dkey1 = maxDex1 - 4;
        int dkey2 = maxDex2 - 4;
        if (maxDex1 < 4) {
            dkey1 = 26 - ( 4 - maxDex1);
        }
        if(maxDex2 < 4) {
            dkey2 = 26-(4-maxDex2);
        }
        System.out.println(dkey1+","+dkey2);
        CaesarCipherTwo caesarTwo = new CaesarCipherTwo(26-dkey1,26-dkey2);
        //System.out.println(caesarTwo.encrypt(input));
        return caesarTwo.encrypt(input);
    }
    public static void main(String args[]) {
        TestCaesarCipherTwo test = new TestCaesarCipherTwo();
        test.SampleTests();
    }
}
