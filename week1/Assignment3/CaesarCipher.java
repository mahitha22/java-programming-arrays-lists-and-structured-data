
/**
 * Write a description of CaesarCipher here.
 * 
 * @version 11/6
 */
import edu.duke.*;
public class CaesarCipher {
    public String encrypt(String input, int key) {
        String alphabetU = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String alphabetL = "abcdefghijklmnopqrstuvwxyz";
        String keyAlphabetU = alphabetU.substring(key) + alphabetU.substring(0,key);
        String keyAlphabetL = alphabetL.substring(key) + alphabetL.substring(0,key);
        StringBuilder encrypted = new StringBuilder(input);
        for(int i = 0; i < encrypted.length(); i++) {
            char currChar = encrypted.charAt(i);
            if(Character.isUpperCase(currChar)) {
                int index = alphabetU.indexOf(currChar);
                if(index != -1) {
                    char newChar = keyAlphabetU.charAt(index);
                    encrypted.setCharAt(i,newChar);
                }
            }
            else {
                int index = alphabetL.indexOf(currChar);
                if(index != -1) {
                    char newChar = keyAlphabetL.charAt(index);
                    encrypted.setCharAt(i,newChar);
                }
            }
            
        }
        return encrypted.toString();
    }
    public void testEncrypt() {
        String encryptmsg = encrypt("FIRST LEGION ATTACK EAST FLANK!", 23);
        System.out.println("Encrypted message is: "+ encryptmsg);
        System.out.println("Decrypted message is: "+ encrypt(encryptmsg,26-23));
        encryptmsg = encrypt("First Legion", 17);
        System.out.println("Encrypted message is: "+ encryptmsg);
        System.out.println("Decrypted message is: "+ encrypt(encryptmsg,26-17));
        encryptmsg = encryptTwoKeys("First Legion", 23,17);
        System.out.println("Encrypted message is: "+ encryptmsg);
        System.out.println("Decrypted message is: "+ encryptTwoKeys(encryptmsg,26-23,26-17));
        encryptmsg = encrypt("At noon be in the conference room with your hat on for a surprise party. YELL LOUD!", 15);
        System.out.println("Encrypted message is: "+ encryptmsg);
        System.out.println("Decrypted message is: "+ encrypt(encryptmsg,26-15));
        encryptmsg = encryptTwoKeys("At noon be in the conference room with your hat on for a surprise party. YELL LOUD!", 8,21);
        System.out.println("Encrypted message is: "+ encryptmsg);
        System.out.println("Decrypted message is: "+ encryptTwoKeys(encryptmsg,26-8,26-21));
    }
    public void testFileEncrypt() {
        int key = 17;
        FileResource fr = new FileResource();
        String message = fr.asString();
        String encrypted = encrypt(message, key);
        System.out.println("key is " + key + "\n" + encrypted);
    }
    public static void main(String args[]) {
        CaesarCipher cc = new CaesarCipher();
        cc.testEncrypt();
        //cc.testFileEncrypt();
    }
    public String encryptTwoKeys (String input, int key1, int key2) {
        String alphabetU = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String alphabetL = "abcdefghijklmnopqrstuvwxyz";
        String key1AlphabetU = alphabetU.substring(key1) + alphabetU.substring(0,key1);
        String key1AlphabetL = alphabetL.substring(key1) + alphabetL.substring(0,key1);
        String key2AlphabetU = alphabetU.substring(key2) + alphabetU.substring(0,key2);
        String key2AlphabetL = alphabetL.substring(key2) + alphabetL.substring(0,key2);
        StringBuilder encrypted = new StringBuilder(input);
        for (int i = 0; i < encrypted.length(); i+=2) {
            char currChar = encrypted.charAt(i);
            if(Character.isUpperCase(currChar)) {
                int index = alphabetU.indexOf(currChar);
                if(index != -1) {
                    char newChar = key1AlphabetU.charAt(index);
                    encrypted.setCharAt(i,newChar);
                }
            }
            else {
                int index = alphabetL.indexOf(currChar);
                if(index != -1) {
                    char newChar = key1AlphabetL.charAt(index);
                    encrypted.setCharAt(i,newChar);
                }
            }
        }
        for (int i = 1; i < encrypted.length(); i+=2) {
            char currChar = encrypted.charAt(i);
            if(Character.isUpperCase(currChar)) {
                int index = alphabetU.indexOf(currChar);
                if(index != -1) {
                    char newChar = key2AlphabetU.charAt(index);
                    encrypted.setCharAt(i,newChar);
                }
            }
            else {
                int index = alphabetL.indexOf(currChar);
                if(index != -1) {
                    char newChar = key2AlphabetL.charAt(index);
                    encrypted.setCharAt(i,newChar);
            }
            }
        }
        return encrypted.toString();
    }
}
