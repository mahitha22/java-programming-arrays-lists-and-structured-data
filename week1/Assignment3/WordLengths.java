
/**
 * 
 * 
 * 
 * @version 11/07
 */
import edu.duke.*;
public class WordLengths {
    void countWordlengths(FileResource resource, int[] counts, String[] stringArray) {
        for(String word : resource.words()) {
            int length = word.length();
            if(!(Character.isLetter(word.charAt(0))) || !(Character.isLetter(word.charAt(length-1)))) {
                    length = length-1;
            }            
            if(length >= counts.length) {
                counts[counts.length]++;
            }
            else {
                counts[length]++;
                if(stringArray[length]==null) {
                    stringArray[length] = word;
                }
                else {
                    stringArray[length] += " " + word ;
                }
            }
        }
     }
    void testCountWordLengths () {
        FileResource fr = new FileResource();
        int counts[] = new int[31];
        String[] stringArray = new String[31];
        countWordlengths(fr, counts, stringArray);
        for(int i=0; i<counts.length; i++) {
            if(counts[i] != 0) {
                System.out.println(counts[i]+" words of length " + i +": "+stringArray[i]);
            }
        }
        System.out.println("Max Index: "+indexOfMax(counts));
    }
    int indexOfMax (int[] values) {
        int maxIndex = 0;
        for(int i=0; i <values.length; i++) {
            if(values[i] > values[maxIndex]) {
                maxIndex = i;
            }
        }
        return maxIndex;
    }
    public static void main(String args[]) {
        WordLengths wl = new WordLengths();
        wl.testCountWordLengths();
    }
}
