
/**
 * 
 * 
 * 
 * @version 11/07
 */
import edu.duke.*;
public class CaesarBreaker {
    int[] countLetters(String message) {
        String alph = "abcdefghijklmnopqrstuvwxyz";
        int[] counts = new int[26];
        for(int k=0; k < message.length(); k++) {
            char ch = Character.toLowerCase(message.charAt(k));
            int dex = alph.indexOf(ch);
            if(dex != -1) {
                counts[dex] += 1;
            }
        }
        return counts;
    }
    public int maxIndex(int[] vals) {
        int maxDex = 0;
        for(int k=0; k <vals.length; k++) {
            if( vals[k] > vals[maxDex]) {
                maxDex = k;
            }
        }
        return maxDex;
    }
    public String decrypt () {
        CaesarCipher cc = new CaesarCipher();
        String encrypted = cc.encrypt("Just a test string with lots of eeeeeeeeeeeeeeeees", 23);
        System.out.println(encrypted);
        int[] freqs = countLetters(encrypted);
        int maxDex = maxIndex(freqs);
        int dkey = maxDex - 4;
        if (maxDex < 4) {
            dkey = 26 - (4-maxDex);
        }   
        return cc.encrypt(encrypted, 26 - dkey);
    }
    public String halfOfString(String message, int start) {
        String temp = "";
        for(int i = start; i < message.length(); i += 2) {
            temp += message.charAt(i);
        }
        return temp;
    }
    public int getKey(String s) {
        int frequencies[] = countLetters(s);
        int maxindex = maxIndex(frequencies);
        return maxindex;
    }
    public String decryptTwokeys(String encrypted) {
        CaesarCipher cc = new CaesarCipher();
        //String encrypted = cc.encryptTwoKeys("Just a test string with lots of eeeeeeeeeeeeeeeees", 23,2);
        //System.out.println(encrypted);
        String firstHalf = halfOfString(encrypted,0);
        String secondHalf = halfOfString(encrypted, 1);
        int maxDex1 = getKey(firstHalf);
        int maxDex2 = getKey(secondHalf);
        int dkey1 = maxDex1 - 4;
        int dkey2 = maxDex2 - 4;
        if (maxDex1 < 4) {
            dkey1 = 26 - ( 4 - maxDex1);
        }
        if(maxDex2 < 4) {
            dkey2 = 26-(4-maxDex2);
        }
        System.out.println(dkey1+","+dkey2);
        return cc.encryptTwoKeys(encrypted, 26-2, 26-20);
    }
    public void testDecrypt() {
        System.out.println("The decrypted message: "+decrypt());
        System.out.println("The decrypted message: "+ decryptTwokeys("Top ncmy qkff vi vguv vbg ycpx"));
    }
    public void testDecryptFile() {
        FileResource fr = new FileResource();
        String message = fr.asString();
        System.out.println("the decrypted file: "+ decryptTwokeys(message));
    }
    public static void main(String args[]) {
        CaesarBreaker cb = new CaesarBreaker();
        cb.testDecrypt();   
    }
    public void testhalfofString() {
        System.out.println(halfOfString("Qbkm Zgis" , 0));
        System.out.println(halfOfString("Qbkm Zgis" , 1));
    }
}
