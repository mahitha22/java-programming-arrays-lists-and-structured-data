
/**
 * * @version 11/06
 */
public class WordPlay {
    public boolean isVowel(char ch) {
       char chr = Character.toLowerCase(ch);
        if(chr == 'a' || chr == 'e' || chr == 'i' || chr == 'o' || chr == 'u') {
            return true;
        }
        return false;
    }
    public String replaceVowels(String phrase, char ch) {
        StringBuilder sb = new StringBuilder(phrase);
        for(int i = 0; i < sb.length(); i++) {
            if(isVowel(phrase.charAt(i))) {
                sb.setCharAt(i,ch);
            }
        }
        return sb.toString();
    }
    public void emphasize(String phrase, char ch) {
        StringBuilder sb = new StringBuilder(phrase);
        for(int i = 0; i < sb.length(); i++) {
            if(Character.toLowerCase(phrase.charAt(i)) == ch) {
                if(i % 2 == 0) {
                    sb.setCharAt(i,'*');
                }
                else {
                    sb.setCharAt(i,'+');
                }
            }
        }
        System.out.println(sb);
    }
   
}
